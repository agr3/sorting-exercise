## considerations

- The ideal solution to managing petabytes or hexabytes of data is using distrubuting computing and big data tools like Hadoop and/or Spark. Hadoop uses a similar solution to "external merge sort" but it adds clusters of nodes to the equation.

- The solution applied in this repository uses an embedded database - SQLite - for simplicity and efficiency.

- The inserts to sqlite DB were made in chunks usings batch inserts

- Memory validations were applied when generating the logSources and before inserting the chunks to the database to prevent memory leaks.

- The npm start script was modified to simulate low memory conditions and to allow calling the GC manually.

- An environment variable in the .env file "MAX_MEMORY" was added to prevent surpassing the node.js "max-old-space-size" config. The MAX_MEMORY should be lower than the max-old-space-size to prevent memory leaks.

- The async solution seems to perform slower in the context of a single machin using a single thread due to the overhead of managing async calls that perform some medium/heavy computation with just 1 thread. The async solution does not have memory checks; memory leaks can occur. 

- Printig "all" the results - as asked in the instructions - is unnecesary in my opinion and conflicts with the execution time for getting the logs from all the sources and ordering the data, since printing to the terminal causes a big overhead. Printing the results is related to the presentation, rather than the process for the solution. I Added the execution time just after printing the first 20 results, which should be enough to better appreciate the time that the process took to get the data and perform the ordering.
