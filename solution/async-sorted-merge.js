"use strict";
const strat = require("./sqliteStrategy");
// Print all entries, across all of the *async* sources, in chronological order.
const strategy = require("./sqliteStrategy");

module.exports = (logSources, printer) => {
  return new Promise(async (resolve, reject) => {

    await strategy.executeAsync(logSources,printer);

    resolve(console.log("Async sort complete."));
  });
};
