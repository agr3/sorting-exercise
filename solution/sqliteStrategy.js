"use strict";
const db = require('better-sqlite3')('./logs.db');
const promise = require('bluebird/js/release/promise');
// Print all entries, across all of the sources, in chronological order.
const readlineSync = require('readline-sync');

function SQLiteStrategy() {

 
    function getMemoryUsed() {

        let memoryUsed = process.memoryUsage();
        for (let key in memoryUsed) {
            memoryUsed[key] = Math.round(memoryUsed[key] / 1024 / 1024 * 100) / 100
        }

        return memoryUsed;
    }

    let startTime = null;
    let inserts = "";
    this.execute = function (logSources, printer) {

        db.prepare("DROP TABLE if exists logs;").run();
        db.prepare("VACUUM;").run();
        db.prepare("CREATE TABLE if not exists logs(message text , date numeric);").run();

        startTime = new Date();
        inserts = "BEGIN;";
    
        for (let x = 0; x < logSources.length; x++) {
            while (true) {

                const result = logSources[x].pop();

                if (result != false) {

                    inserts += `INSERT INTO logs (message,date) VALUES  ('${result.msg}',${result.date.getTime()});`;
                    let memoryUsed = getMemoryUsed();
                    if (memoryUsed.heapUsed > process.env.MAX_MEMORY) {
                        saveData(inserts);
                        clearMemory();
                       
                    }
                }
                else
                    break;
            }

        }

        if (inserts != "") {
            inserts += "END;";
            saveData(inserts);
            clearMemory();
        }

        orderData(printer);


    }

    function clearMemory() {

        inserts = "";
        global.gc();

    };

    function saveData(inserts) {

        db.exec(inserts);
    };

    this.executeAsync = async function (logSources, printer, popMethod) {

        db.prepare("DROP TABLE if exists logs;").run();
        db.prepare("VACUUM;").run();
        db.prepare("CREATE TABLE if not exists logs(message text , date numeric);").run();
    

        const logs = [];
        let inserts = "BEGIN;";
        let allDrained = false;

        const promises = []

        for (let x = 0; x < logSources.length; x++) {

            promises.push(new Promise(async (res, rej) => {

                while (true) {

                    const result = await logSources[x].popAsync();

                    if (result == false)
                        res();
                    else {
                        logs.push(result);
                        inserts += `INSERT INTO logs (message,date) VALUES  ('${result.msg}',${result.date.getTime()});`;
                    }

                }

            }));
        }

        await Promise.all(promises);
        inserts += "END;";

        db.exec(inserts);

        orderData(printer);

    }

    function orderData(printer) {

        let cont = 1;
        const stmt = db.prepare('SELECT * FROM logs order by date asc');
        let userAnswer = "";
        let iterator = stmt.iterate();
        let { value: log } = iterator.next();

        while (true) {

            if (cont % 20 == 0) {

                userAnswer = readlineSync.question('Keep showing results? y/n ');
            }

            if (userAnswer.toLowerCase() == "n")
                break;

            while (true) {

                printer.print({ msg: log.message, date: new Date(log.date) });
                log = iterator.next().value;
                cont++;
                if (cont % 20 == 0) {
                    break;

                }

            }

            if (cont == 20) {
                const endTime = new Date();

                console.log("Execution time: ",(endTime - startTime) / 1000);
            }

        }

        iterator.return();

    }

}

const strat = new SQLiteStrategy();


module.exports = strat;