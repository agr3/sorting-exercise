"use strict";

const strategy = require("./sqliteStrategy");

module.exports = (logSources, printer) => {

  strategy.execute(logSources, printer);
  return console.log("Sync sort complete.");

};


